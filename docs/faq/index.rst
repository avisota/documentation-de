.. _faq:

Häufig gestellte Fragen
=======================

.. _faq_status:

Status Information
------------------

.. cssclass:: sidebar
.. compound::

   **Status**

   .. figure:: https://poser.pugx.org/avisota/core/v/stable.png
      :alt: Latest Stable Version
      :target: https://packagist.org/packages/avisota/core

   .. figure:: https://poser.pugx.org/avisota/core/v/unstable.png
      :alt: Latest Unstable Version
      :target: https://packagist.org/packages/avisota/core

   .. figure:: https://poser.pugx.org/avisota/core/downloads.png
      :alt: Total Downloads
      :target: https://packagist.org/packages/avisota/core

   .. figure:: https://poser.pugx.org/avisota/core/license.png
      :alt: License
      :target: https://packagist.org/packages/avisota/core

Auf einigen Seiten auf denen Komponenten / Pakete dokumentiert sind, steht häufig eine Statusinformation am Seitenrand (Beispiel rechts).

Dieser Block informiert über den aktuellen Entwicklungsstand der Komponente / des Pakets.

Das Badge |badge stable| zeigt die jüngste veröffentlichte, stabile Version an.

Das Badge |badge unstable| zeigt die jüngste veröffentlichte, instabile Version (RC, beta, alpha, dev) an.

Das Badge |badge downloads| zeigt die Anzahl der Downloads die bei `Packagist.org <http://packagist.org>`_ erfasst wurden.

Das Badge |badge license| zeigt die Lizenz an, unter der die Komponente / das Paket steht.


.. |badge stable| image:: https://poser.pugx.org/avisota/core/v/stable.png
   :alt: Latest Stable Version
   :target: https://packagist.org/packages/avisota/core

.. |badge unstable| image:: https://poser.pugx.org/avisota/core/v/unstable.png
   :alt: Latest Unstable Version
   :target: https://packagist.org/packages/avisota/core

.. |badge downloads| image:: https://poser.pugx.org/avisota/core/downloads.png
   :alt: Total Downloads
   :target: https://packagist.org/packages/avisota/core

.. |badge license| image:: https://poser.pugx.org/avisota/core/license.png
   :alt: License
   :target: https://packagist.org/packages/avisota/core

.. _faq_tracker:

Tracker
-------

TODO

.. _faq_sources:

Sources
-------

TODO
