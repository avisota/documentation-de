Avisota Handbuch
================

Dies ist das offizielle Avisota Handbuch und Dokumentation.

.. ref: `Dokumentation zum Framework <framework-dokumentation>`

.. |framework-dokumentation| replace:: **Framework**
.. _framework-dokumentation: framework/

.. |contao-dokumentation| replace:: **Contao CMS Integration**
.. _contao-dokumentation: contao/

.. |bundle-dokumentation| replace:: **Symfony2 Bundle**
.. _bundle-dokumentation: bundle/

.. |faq| replace:: **Häufig gestellte Fragen**
.. _faq: faq/

.. cssclass:: row
.. compound::

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |framework-dokumentation|_

         .. cssclass:: panel-body
         .. compound::

            Das Framework bietet eine vielzahl von Möglichkeiten und Funktionen um das Handling mit Nachrichten zu
            vereinfachen. Dabei setzt das Framework auf die `swiftmailer <http://swiftmailer.org/>`_ Bibliothek und
            ergänzt diese um zusätzliche Funktionen.

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |contao-dokumentation|_

         .. cssclass:: panel-body
         .. compound::

            Die Integration in das `Contao Open Source CMS <http://contao.org>`_ ist die erste und umfassenste Umsetzung
            von Avisota für ein Dritt-System.

.. cssclass:: row
.. compound::

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |bundle-dokumentation|_

         .. cssclass:: panel-body
         .. compound::

            Das `Symfony2 <http://symfony.com>`_ Bundle bietet eine einfache Integration des Frameworks in das Symfony2 Framework.

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |faq|_

         .. cssclass:: panel-body
         .. compound::

            Häufig gestellte Fragen zum Projekt werden im FAQ beantwortet.

Table of Contents
-----------------

.. toctree::
   :maxdepth: 10
   :titlesonly:

   framework/index
   contao/index
   bundle/index
   faq/index

Index und Verzeichnis
---------------------

* :ref:`genindex`
* :ref:`search`
