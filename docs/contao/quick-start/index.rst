.. _contao-quick-start:

.. role:: check
   :class: glyphicon glyphicon-ok-circle text-success
.. role:: edit
   :class: glyphicon glyphicon-edit small text-info
.. role:: right
   :class: glyphicon glyphicon-chevron-right small
.. role:: text-success

Quick Start Checkliste
======================

Hier folgt eine einfache Checkliste für einen Schnellstart innerhalb von 15 Minuten. Hier werden nur die wichtigsten
Einstellungen getroffen, für eine detaillierte Konfiguration lesen Sie die Dokumentation zur :doc:`Konfiguration
<../konfiguration/index>`, :doc:`Mailings verwalten <../mailings/index>` und :doc:`Frontend Integration
<../frontend/index>`.

-----

Avisota-Newsletter :right:`>` Einstellungen :right:`>` Sendeeinstellungen :right:`>` **Transport**

- :check:`X` Neues Transportmodul anlegen

  - :edit:`X` **Titel:** Swift

  - :edit:`X` **Transporttyp:** Swift PHP Mailer

  - :edit:`X` **Absenderadresse:** *eigene Absender-E-Mail-Adresse eintragen*

  - :edit:`X` **Absendername:** *eigenen Absender-Namen eintragen*

- :check:`X` Wenn Versand über SMTP erforderlich ist

  - :edit:`X` **Mails per SMTP senden:** Mails per SMTP versenden

  - :edit:`X` **SMTP-Hostname:** *eigene SMTP Serveradresse eintragen*

  - :edit:`X` **SMTP-Benutzername:** *eigenen SMTP Benutzernamen eintragen*

  - :edit:`X` **SMTP-Passwort:** *eigenes SMTP Passwort eintragen*

  - :edit:`X` **SMTP-Verschlüsselung:** *verwendete SMTP Verschlüsselung auswählen*

  - :edit:`X` **SMTP-Portnummer:** *eigenen SMTP Port eintragen*

-----

Avisota-Newsletter :right:`>` Einstellungen :right:`>` Sendeeinstellungen :right:`>` **Warteschlangen**

- :check:`X` Neue Warteschlange anlegen

  - :edit:`X` **Typ:** Einfache datenbankbasierte Warteschlange

  - :edit:`X` **Titel:** Warteschlange

  - :edit:`X` **Transportmodul:** *Swift auswählen*

  - :edit:`X` **Tabellenname:** avisota_queue

  - :edit:`X` **Manuellen Versand erlauben:** *aktivieren*

-----

Avisota-Newsletter :right:`>` Einstellungen :right:`>` Einstellungen :right:`>` **Anrede**

- :check:`X` Neue Standardgruppe erzeugen

-----

**Seitenstruktur**

- :check:`X` Neue Seite anlegen

  - :edit:`X` **Seitenname:** An- und Abmeldeformular

  - :edit:`X` **Seitentyp:** Reguläre Seite

-----

Avisota-Newsletter :right:`>` Einstellungen :right:`>` Empfängereinstellungen :right:`>` **Empfängerquellen**

- :check:`X` Neue Empfängerquelle anlegen

  - :edit:`X` **Typ:** Integrierte Abonnentenverwaltung

  - :edit:`X` **Name:** Integrierte Empfänger

  - :edit:`X` **Anrede:** *Default group generated at X auswählen*

  - :edit:`X` **Verwaltungsseite:** *Seite "An- und Abmeldeformular" auswählen*

-----

Avisota-Newsletter :right:`>` Einstellungen :right:`>` Empfängereinstellungen :right:`>` **Mailinglisten**

- :check:`X` Neue Mailingliste anlegen

  - :edit:`X` **Name:** Newsletter

-----

Avisota-Newsletter :right:`>` Einstellungen :right:`>` Nachrichteneinstellungen :right:`>` **Themes**

- :check:`X` Neues Theme anlegen

  - :edit:`X` **Titel:** Newsletter

-----

Avisota-Newsletter :right:`>` Einstellungen :right:`>` Nachrichteneinstellungen :right:`>` Themes :right:`>` **Layouts**

- :check:`X` Neues Layout anlegen

  - :edit:`X` **Typ:** MailChimp Blueprints

  - :edit:`X` **Titel:** Anmelde E-Mail

  - :edit:`X` **Mailchimp-Template:** *Transactional [HTML] auswählen*

- :check:`X` Neues Layout anlegen

  - :edit:`X` **Typ:** MailChimp Blueprints

  - :edit:`X` **Titel:** Standardlayout

  - :edit:`X` **Mailchimp-Template:** *1 column - 2 columns [HTML] auswählen*

-----

Avisota-Newsletter :right:`>` Einstellungen :right:`>` Einstellungen :right:`>` **Avisota-Systemeinstellungen**

- :edit:`X` **Standard-Transportmodul:** *Swift auswählen*

-----

Avisota-Newsletter :right:`>` **Nachrichten**

- :check:`X` Neue Kategorie anlegen

  - :edit:`X` **Titel:** Vorlagen

  - :edit:`X` **Beinhaltet Boilerplate:** *aktivieren*

-----

Avisota-Newsletter :right:`>` Nachrichten :right:`>` **Vorlagen**

- :check:`X` Neuen Newsletter anlegen

  - :edit:`X` **Betreff:** Anmelde-Bestätigung

  - :edit:`X` **Layout:** *Anmelde E-Mail auswählen*

- :check:`X` Neuen Newsletter anlegen

  - :edit:`X` **Betreff:** Abmelde-Bestätigung

  - :edit:`X` **Layout:** *Standardlayout auswählen*

-----

Avisota-Newsletter :right:`>` Nachrichten :right:`>` Vorlagen :right:`>` **Mailing: Anmelde-Bestätigung**

- :check:`X` Neues Element anlegen

  - :edit:`X` **Zelle:** Linke Spalte

  - :edit:`X` **Element-Typ:** Text

  - :edit:`X` **Text:** Vielen Dank für Ihre Anmeldung zu unserem Newsletter,
    Sie müssen Ihr Abonnement noch bestätigen.

-----

Avisota-Newsletter :right:`>` Nachrichten :right:`>` Vorlagen :right:`>` **Mailing: Abmelde-Bestätigung**

- :check:`X` Neues Element anlegen

  - :edit:`X` **Zelle:** Hauptspalte

  - :edit:`X` **Element-Typ:** Text

  - :edit:`X` **Text:** Vielen Dank für Ihr Interesse,
    wir hoffen Sie bald wieder als Abonnent begrüßen zu dürfen.

-----

Layout :right:`>` Themes :right:`>` **Frontend-Module**

- :check:`X` Neue Modul anlegen

  - :edit:`X` **Titel:** Avisota - Anmelde Formular

  - :edit:`X` **Modultyp:** *Empfänger - Bestellung* auswählen

  - :edit:`X` **Ausgewählte/Auswählbare Mailinglisten:** *Newsletter auswählen*

  - :edit:`X` **Bestätigungsnachricht:** *Anmelde-Bestätigung auswählen*

- :check:`X` Neue Modul anlegen

  - :edit:`X` **Titel:** Avisota - Abmelde Formular

  - :edit:`X` **Modultyp:** *Empfänger - Abmelden* auswählen

  - :edit:`X` **Ausgewählte/Auswählbare Mailinglisten:** *Newsletter auswählen*

  - :edit:`X` **Bestätigungsnachricht:** *Abmelde-Bestätigung auswählen*

-----

Inhalte :right:`>` Artikel :right:`>` Seite: An- und Abmeldeformular :right:`>` **Artikel: An- und Abmeldeformular**

- :check:`X` Neues Element anlegen

  - :edit:`X` **Elementtyp:** Modul

  - :edit:`X` **Modul:** *Avisota - Anmelde Formular auswählen*

- :check:`X` Neues Element anlegen

  - :edit:`X` **Elementtyp:** Modul

  - :edit:`X` **Modul:** *Avisota - Abmelde Formular auswählen*

-----

Avisota-Newsletter :right:`>` **Nachrichten**

- :check:`X` Neue Kategorie anlegen

  - :edit:`X` **Titel:** Newsletter

-----

Avisota-Newsletter :right:`>` Nachrichten :right:`>` **Newsletter**

- :check:`X` Neuen Newsletter anlegen

  - :edit:`X` **Betreff:** Der erste Newsletter

  - :edit:`X` **Layout:** *Standardlayout auswählen*


Avisota-Newsletter :right:`>` Nachrichten :right:`>` Newsletter :right:`>` **Der erste Newsletter**

- :check:`X` Neues Element anlegen

  - :edit:`X` **Zelle:** Hauptspalte

  - :edit:`X` **Element-Typ:** Text

  - :edit:`X` **Text:** Das ist der erste Newsletter, viel Spaß :-)
