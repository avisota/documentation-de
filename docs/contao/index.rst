.. _contao-dokumentation:

Contao CMS Integration
======================

Die Integration für das `Contao Open Source CMS <https://contao.org>`_ ist die umfassenste Integration des
Avisota Frameworks in ein Dritt-System.

.. |grundlagen| replace:: **Grundlagen**
.. _grundlagen: grundlagen/

.. |installation| replace:: **Installation**
.. _installation: installation/

.. |quick-start| replace:: **Quick-Start Checkliste**
.. _quick-start: quick-start/

.. |konfiguration| replace:: **Konfiguration**
.. _konfiguration: konfiguration/

.. |mailings| replace:: **Mailings erstellen und versenden**
.. _mailings: mailings/

.. |referenz| replace:: **Referenz**
.. _referenz: referenz/

.. |support| replace:: **Support**
.. _support: support/

.. cssclass:: row
.. compound::

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |grundlagen|_

         .. cssclass:: panel-body
         .. compound::

            Erfahre mehr über das System, seinen Aufbau, die Abhängigkeiten und Details der internen
            Abläufe in unserem *Grundlagen* Abschnitt.

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |installation|_

         .. cssclass:: panel-body
         .. compound::

            Die Installation kann via Composer oder von Hand durchgeführt werden.

.. cssclass:: row
.. compound::

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |quick-start|_

         .. cssclass:: panel-body
         .. compound::

            Für die ganz schnelle Einrichtung gibt es eine Checkliste was zu tun ist, um das System in ca. 15 Minuten
            in einen benutzbaren Zustand zu bringen.

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |konfiguration|_

         .. cssclass:: panel-body
         .. compound::

            Die Konfiguration ist umfangreich, aber mit einem guten Leitfaden schnell zu bewältigen.

.. cssclass:: row
.. compound::

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |mailings|_

         .. cssclass:: panel-body
         .. compound::

            Wie erstellt man Mailings und wie versendet man diese?! Hier gibt es die Antwort!

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |support|_

         .. cssclass:: panel-body
         .. compound::

            Support erhalten oder Support geben, hier erfährst du wo und wie es geht.

.. cssclass:: row
.. compound::

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |referenz|_

         .. cssclass:: panel-body
         .. compound::

            Referenzliste zu Komponenten, Events und vieles mehr findest du hier.

Table of Contents
-----------------

.. toctree::
   :maxdepth: 5
   :titlesonly:

   grundlagen/index
   installation/index
   quick-start/index
   konfiguration/index
   mailings/index
   support/index
   referenz/index
