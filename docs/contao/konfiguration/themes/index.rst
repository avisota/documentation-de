.. _contao-konfiguration-themes:

.. role:: text-info

Themes
======

Themes in Avisota 2 unterscheiden sich kaum von Themes in Contao. Sie bestehen ebenfalls aus einem Template Verzeichnis
und mehreren Layouts für die Mailings.

.. figure:: einstellungen.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Grundkonfiguration
------------------

Mit dem Button :text-info:`Neues Theme` wird eine neues Theme angelegt, jedes Theme erhält einen :text-info:`Namen`
und einen :text-info:`Alias`. Optional kann auch noch ein Templateverzeichnis ausgewählt werden, in dem die angepassten
Templates hinterlegt werden.

.. figure:: theme-bearbeiten.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Layouts bearbeiten
------------------

Um die Layouts eines Themes zu bearbeiten, muss man auf den Button :text-info:`Layouts bearbeiten` ganz rechts in der
Theme Auflistung klicken.

.. figure:: theme-layouts.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Mehr zum Thema Layouts findest du in dem Unterabschnitt :doc:`Layouts <layouts/index>`.

Table of Contents
-----------------

.. toctree::
   :maxdepth: 2
   :titlesonly:

   layouts/index
