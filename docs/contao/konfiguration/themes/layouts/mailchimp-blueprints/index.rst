.. _contao-konfiguration-theme-layout-mailchimp-blueprints:

.. role:: text-info

MailChimp Blueprints
====================

Basierend auf den `HTML Templates von MailChimp <https://github.com/mailchimp/Email-Blueprints>`_ erzeugt dieser
Layout-Typ HTML E-Mails. Besonders zu beachten ist, dass es 2 Arten von Templates gibt. Die :text-info:`Transactional`
und die :text-info:`Nicht Transactional` Templates.

Um kurz den Unterschied zu erläutern, eine :text-info:`Transactional` E-Mail ist eine Bestätigungs E-Mail mit Aktion
wie sie bspw. beim Abonnieren für das Double-Opt-In Verfahren versendet wird. Eine :text-info`Nicht Transactional`
E-Mail ist entsprechend eine nicht-Bestätigungs E-Mail oder korrekterweise eine ganz reguläre E-Mail mit Inhalt.

.. cssclass:: alert alert-info
.. compound::

   Für die Anmeldung mittels Double-Opt-In sollte man ein Layout anlegen, dass ein :text-info:`Transactional` Template
   verwendet.

   .. figure:: template-transactional.png
      :scale: 100%
      :class: img-thumbnail img-responsive

-----

Stylesheets
-----------

In den :text-info:`Template-Einstellungen` können die Stylesheets, die für dieses Layout gelten sollen ausgewählt
werden. Primär werden hier die Stylesheets aus den Contao Themes angeboten. Es gibt aber auch eine experimentelle
Unterstützung von Stylesheets die in Theme+ gepflegt werden.

Die Option :text-info:`Globales Stylesheet entfernen` bezieht sich auf die Styles, die bereits in den HTML Templates
definiert sind. Diese sind als Grundlage nicht zu verachten, wer auf diese allerdings verzichten möchte, aktiviert
diese Option.

.. figure:: stylesheets.png
   :scale: 100%
   :class: img-thumbnail img-responsive

-----

Struktur und Inhalt
-------------------

Im Abschnitt :text-info:`Struktur und Inhalt` kann man für die einzelnen Abschnitte / Spalten in dem Template die
erlaubten Inhaltselemente festlegen. Damit lässt sich abhängig vom Layout eine einfache Limitierung für die Redakteure
umsetzen.

.. figure:: struktur-und-inhalt.png
   :scale: 100%
   :class: img-thumbnail img-responsive

