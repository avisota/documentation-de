.. _contao-konfiguration-theme-layouts:

.. role:: text-info

Layouts
=======

Mit Hilfe von Layouts wird das Design und das Verhalten beim erzeugen des Mailings bestimmt. Layouts können von
Klartext, über HTML, bis hin zu PDF oder RTF Dokumenten alles erzeugen.

.. |generischer-renderer| replace:: **Generischer Renderer**
.. _generischer-renderer: generischer-renderer/

.. |mailchimp-blueprints| replace:: **MailChimp Blueprints**
.. _mailchimp-blueprints: mailchimp-blueprints/

.. cssclass:: row
.. compound::

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |generischer-renderer|_

         .. cssclass:: panel-body
         .. compound::

            Der :text-info:`Generische Renderer` wird primär zum erzeugen der Inhalte im Backend verwendet.
            Jedes Inhaltselement hat einen solchen Renderer, der sozusagen das Standard-Design erzeugt.
            Dieser Layout-Typ eignet sich aber nicht nur für das Backend, auch für die Website Integration ist er
            bestens geeignet.

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |mailchimp-blueprints|_

         .. cssclass:: panel-body
         .. compound::

            Die :text-info:`MailChimp Blueprints` sind ein Set aus vorgefertigten, stabilen E-Mail HTML Templates
            die unter anderem von MailChimp als Grundlage für Ihre Mailings verwendet werden. Der :text-info:`MailChimp
            Blueprints` Layout-Typ verwendet diese besagten HTML Templates und erzeugt damit die Mailings.

Table of Contents
-----------------

.. toctree::
   :maxdepth: 2
   :titlesonly:

   generischer-renderer/index
   mailchimp-blueprints/index
