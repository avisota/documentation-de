.. _contao-konfiguration-theme-layout-generischer-renderer:

.. role:: text-info

Generischer Renderer
====================

Der :text-info:`Generische Renderer` wird primär zum erzeugen der Inhalte im Backend verwendet. Jedes Inhaltselement
hat einen solchen Renderer, der sozusagen das Standard-Design erzeugt. Dieser Layout-Typ eignet sich aber nicht nur
für das Backend, auch für die Website Integration ist er bestens geeignet.

.. cssclass:: alert alert-warning::

**Hinweis:** Mit dem :text-info:`Generischen Renderer` lassen sich keine vollständigen E-Mails erzeugen, sondern nur
deren Inhalte!
