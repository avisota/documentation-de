.. _contao-konfiguration:

.. role:: glyphicon-chevron-right
   :class: glyphicon glyphicon-chevron-right small

Konfiguration
=============

.. cssclass:: row
.. compound::

   .. cssclass:: col-md-6
   .. compound::

      .. figure:: einstellungen.png
         :scale: 100%
         :class: img-thumbnail img-responsive

   .. cssclass:: col-md-6
   .. compound::

      Avisota 2 besteht aus zahlreichen Komponenten, die alle einzeln konfiguriert werden können.
      Unter dem Hauptmenüpunkt *Einstellungen* findet man eine Übersicht, aller konfigurierbaren Komponenten.

      Hier können die einzelnen Komponenten konfiguriert werden.

      .. cssclass:: alert alert-info

         **Tipp:** In der Regel bauen die Einstellungen die weiter oben gelistet sind, auf denen auf die die weiter
         unten gelistet sind. Es empfiehlt sich daher, bei der initialen Konfiguration unten anzufangen und sich nach
         oben durch zu arbeiten.

.. |transport| replace:: **Transport**
.. _transport: transport/

.. |warteschlangen| replace:: **Warteschlangen**
.. _warteschlangen: warteschlangen/

.. |mailinglisten| replace:: **Mailinglisten**
.. _mailinglisten: mailinglisten/

.. |empfängerquellen| replace:: **Empfängerquellen**
.. _empfängerquellen: empfaengerquellen/

.. |themes| replace:: **Themes**
.. _themes: themes/

.. |layouts| replace:: Themes :glyphicon-chevron-right:`>` **Layouts**
.. _layouts: themes/layouts/

.. |anreden| replace:: **Anreden**
.. _anreden: anreden/

.. |systemeinstellungen| replace:: **Allgemeine Systemeinstellungen**
.. _systemeinstellungen: systemeinstellungen/

.. cssclass:: row
.. compound::

   .. cssclass:: col-md-4
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |transport|_

         .. cssclass:: panel-body
         .. compound::

            Wie werden Nachrichten versendet? SMTP? PHP Mail? Oder Webservice?
            Hier wird die Versandmethode konfiguriert.

   .. cssclass:: col-md-4
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |warteschlangen|_

         .. cssclass:: panel-body
         .. compound::

            Nachrichten werden vor dem Versand in einer Warteschlange geparkt.
            Die Warteschlange kann dann Manuell, via Cron oder dezentral versendet werden.

   .. cssclass:: col-md-4
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |mailinglisten|_

         .. cssclass:: panel-body
         .. compound::

            Kategorien, Gruppen, Interessensgebiete. Es gibt viele Dinge, die man abonnieren möchte.
            Als Mailinglisten kann ein Interessent seine jeweiligen gewünschten Mailings abonnieren.

.. cssclass:: row
.. compound::

   .. cssclass:: col-md-4
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |empfängerquellen|_

         .. cssclass:: panel-body
         .. compound::

            Woher kommen die Abonnenten? Und an wen möchte ich eigentlich genau versenden?
            Mit den Empfängerquellen lässt sich konfigurieren, wer die Nachricht bekommen soll.

   .. cssclass:: col-md-4
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |themes|_

         .. cssclass:: panel-body
         .. compound::

            Themes sind eine autarke Sammlung von Templates und Layouts für die Gestalltung von Mailings.

   .. cssclass:: col-md-4
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |layouts|_

         .. cssclass:: panel-body
         .. compound::

            Layouts gehören zu den Themes. Sie spezifizieren wie ein Mailing generiert wird und damit auch das
            Aussehen.

.. cssclass:: row
.. compound::

   .. cssclass:: col-md-4
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |anreden|_

         .. cssclass:: panel-body
         .. compound::

            xxx

   .. cssclass:: col-md-4
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |systemeinstellungen|_

         .. cssclass:: panel-body
         .. compound::

            xxx

Table of Contents
-----------------

.. toctree::
   :maxdepth: 2
   :titlesonly:

   transport/index
   warteschlangen/index
   empfaengerquellen/index
   mailinglisten/index
   themes/index
   anreden/index
   systemeinstellungen/index
