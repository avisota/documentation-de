.. _contao-konfiguration-transport:

.. role:: text-info

Transportmodule
===============

Ein :text-info:`Transport` kümmert sich um den Versand einer Nachricht. Für den Anfang starten wir mit einem SMTP
Versand, dazu wählt man links aus dem Menü :text-info:`Avisota-Newsletter > Einstellungen` und aus den Einstellungen
:text-info:`Transport`.

.. figure:: einstellungen.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Grundkonfiguration
------------------

Mit dem Button :text-info:`Neues Transportmodul` wird ein neues Transportmodul angelegt, als Transporttyp muss
:text-info:`Swiftmailer` ausgewählt werden. Außerdem muss das Transportmodul benannt werden.

.. figure:: typ-und-titel.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Absende- und Antwortadressen
----------------------------

Im Abschnitt :text-info:`Details für Absende- und Antwortadressen` muss die Absenderadresse (`From Header
<http://de.wikipedia.org/wiki/Header_(E-Mail)#From:_Absender>`_) hinterlegt werden. Außerdem kann eine Senderadresse
(`Sender Header <http://de.wikipedia.org/wiki/Header_(E-Mail)#Sender:_Technischer_Absender>`_) und eine Antwortadresse
(`Reply-To Header <http://de.wikipedia.org/wiki/Header_(E-Mail)#Reply-To:_Antwortadresse>`_) hinterlegt werden.

.. figure:: absender-sender-und-antwort-adresse.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Server-Einstellungen
--------------------

Im Abschnitt :text-info:`Swift PHP Mailer` können außerdem die SMTP Einstellungen konfiguriert werden. Hat man seine
SMTP Einstellungen bereits in den Contao Systemeinstellungen hinterlegt, kann man die Option
:text-info:`Systemeinstellungen benutzen` verwenden. Es werden dann die gleichen Einstellungen verwendet,
mit denen auch Contao seine E-Mails versendet.

.. figure:: smtp-systemeinstellungen.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Will man statt dessen aber eigene SMTP Einstellungen verwenden, weil man bspw. über einen separaten Mail-Server
verschicken muss, wählt man :text-info:`Mails per SMTP versenden`. Hier kann man selbst :text-info:`Servername`,
:text-info:`Benutzername`, :text-info:`Passwort`, :text-info:`Verschlüsselung` und :text-info:`Port` für diesen
Transport festlegen.

.. figure:: smtp-benutzerdefinierte-einstellungen.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Für den Fall, dass die Einstellungen Systemweit auf PHP Ebene (``php.ini``) konfiguriert wurden, kann man auch die
Option :text-info:`Mails per PHP-Funktion versenden` wählen. In diesem Fall wird die PHP ``mail()`` Funktion
zum versand verwendet.

.. figure:: php-mail.png
   :scale: 100%
   :class: img-thumbnail img-responsive
