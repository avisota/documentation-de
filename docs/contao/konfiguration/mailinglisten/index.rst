.. _contao-konfiguration-mailingliste:

.. role:: text-info

Mailingliste
============

Oft will man Informationen zu verschiedenen Themen anbieten. Dies können irgendwelche Kategorien, Gruppierungen oder
einfach nur Interessensgebiete sein. Als Abonnent will man natürlich nur die Themen abonnieren, die einen auch
interessieren. Mailinglisten repräsentieren diese Themen und können von Interessenten abonniert und
deabonniert werden.

.. figure:: einstellungen.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Mit dem Button :text-info:`Neue Mailingliste` wird eine neue Mailingliste angelegt.
Mailinglisten haben allerdings nur einen :text-info:`Namen` und einen :text-info:`Alias`.

.. figure:: name-und-alias.png
   :scale: 100%
   :class: img-thumbnail img-responsive
