.. _contao-konfiguration-warteschlangen:

.. role:: text-info
.. role:: glyphicon-ok
   :class: glyphicon glyphicon-ok text-success
.. role:: glyphicon-warning-sign
   :class: glyphicon glyphicon-warning-sign text-warning
.. role:: glyphicon-remove
   :class: glyphicon glyphicon-remove text-danger

Warteschlangen
==============

Warteschlangen dienen dazu, die zu versendenden Nachrichten zwischen zu Speichern, bis diese letztlich versand werden.
Der technische Versand verläuft Asynchron zu Beauftragung des Versands. D.h. klickt ein Redakteur im Backend auf
Versenden, werden die Nachrichten erzeugt und in die Warteschlange abgelegt. Anschließend werden die generierten
Nachrichten entweder durch den Redakteur oder bspw. von einem Cron versendet.

.. figure:: einstellungen.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Grundkonfiguration
------------------

Mit dem Button :text-info:`Neue Warteschlange` wird eine neue Warteschlange angelegt, als erstes muss ein Typ
ausgewählt werden, anschließend bekommt die Warteschlange noch einen Namen.

.. figure:: typ-titel-und-alias.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Der Typ einer Warteschlange hat Einfluss auf die Funktion- und Leistungsfähigkeit beim Versand.
Welche Typen es gibt und was der Vor- / Nachteil der einzelnen Warteschlangentypen sind, wird in den folgenden
Abschnitten erklärt.

.. cssclass:: alert alert-warning

   **Hinweis:** In Version 2.0 steht ausschließlich die `Einfache datenbankbasierte Warteschlange`_ zur
   verfügung. Weitere Implementierungen werden in den kommenden Versionen folgen.

.. figure:: transport-einstellungen.png
   :scale: 100%
   :class: img-thumbnail img-responsive

In den :text-info:`Transport-Einstellungen` wird das Versandverhalten festgelegt. Ganz wichtig ist die Auswahl eines
:text-info:`Transportmodul`s, über den die Mailings aus dieser Warteschlange verschickt werden. Diese Einstellungen
sind vor allem wichtig, um bspw. Limitierungen des Hosters einhalten zu können.

Über :text-info:`Sendedauer` wird festgelegt, wie viele Sekunden ein Versandzyklus maximal dauern darf. Als Standard
wird hier 80% der ``max_execution_time`` verwendet.

Über :text-info:`Anzahl der Sendungen` wird festgelegt, wie viele Mailings maximal in einem Versandzyklus versendet
werden dürfen.

Über :text-info:`Zyklen-Pause` wird festgelegt, wie viele Sekunden Pause zwischen zwei Versandzyklen eingehalten
werden muss.

.. figure:: versand-einstellungen.png
   :scale: 100%
   :class: img-thumbnail img-responsive

In den :text-info:`Versand-Einstellungen` kann man festlegen, ob man den Versand aus dem Backend erlauben möchte
(:text-info:`Manuellen Versand erlauben`). Wird dies aktiviert kann der Redakteur diese Warteschlange im Backend
verwenden.

.. cssclass:: alert alert-warning

   **Hinweis:** In Version 2.0 steht noch kein Cron Versand zur Verfügung, die Option :text-info:`Manuellen
   Versand erlauben` sollte also immer aktiviert werden.

Einfache datenbankbasierte Warteschlange
----------------------------------------

Die :text-info:`Einfache datenbankbasierte Warteschlange` ist wie der Name schon sagt eine Warteschlange, die auf
eine Datenbanktabelle aufsetzt und sehr einfach implementiert ist. Dieser Warteschlangentyp eignet sich vor allem für
einfache Seiten, bei denen der Versand vorwiegend von einem Redakteur durchgeführt wird.

- :glyphicon-ok:`X` Performant dank Datenbankunterstützung.
- :glyphicon-warning-sign:`X` In der jetzigen Implementierung noch nicht für den Cron-Versand geeignet.
- :glyphicon-remove:`X` Bei Mailings mit großen Anhängen kann es zu einer großen Datenbanktabelle führen, weil die
  E-Mail als Rohdaten darin abgelegt wird.
- :glyphicon-remove:`X` Nicht für den verteilten Versand geeignet.

.. figure:: einfache-datenbankbasierte-warteschlange.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Die einzige Einstellungsmöglichkeit für diesen Typ ist der :text-info:`Tabellenname` in dem die Mailings abgelegt
werden. Dieser Tabellennamen kann ein beliebiger, gültiger Datenbanktabellenname sein. Die Tabelle wird automatisch
angelegt.

.. cssclass:: alert alert-danger

   **Hinweis:** Für jede Warteschlange muss eine eigene Tabelle verwendet werden, sonst kann es zu Problemen kommen!
