.. _contao-konfiguration-empfängerquellen:

.. role:: text-info

Empfängerquellen
================

In Avisota 2 werden Mailings grundsätzlich als :text-info:`Empfängerquellen` versendet. Unter einer
:text-info:`Empfängerquelle` kann man sich einen Pool an Empfängern vorstellen. Woher die Empfänger kommen, ist
letztlich vom Typ der :text-info:`Empfängerquelle` abhängig.

.. figure:: einstellungen.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Grundkonfiguration
------------------

Mit dem Button :text-info:`Neue Empfängerquelle` wird eine neue Empfängerquelle angelegt, als erstes muss das
:text-info:`Empfängerquellen-Modul` ausgewählt werden, anschließend bekommt die Empfängerquelle noch einen
:text-info:`Namen` und einen :text-info:`Alias`.

.. figure:: name-alias-und-typ.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Filtereinstellungen
-------------------

Über die text-info:`Filtereinstellungen` lassen sich die bereitgestellten Empfänger noch weiter eingränzen. Welche
Filter zur Verfügung stehen, ist ganz von dem :text-info:`Empfängerquellen-Modul` abhängig.

.. figure:: filter-einstellungen.png
   :scale: 100%
   :class: img-thumbnail img-responsive

Empfängerquellen-Module
-----------------------

Das :text-info:`Empfängerquellen-Modul` ist quasi der Typ der :text-info:`Empfängerquelle`. Die Fähigkeiten sind
alle davon abhängig, welches :text-info:`Empfängerquellen-Modul` ausgewählt wurde. Diese werden in den folgenden
Abschnitten erklärt.

CSV Empfängerquelle
^^^^^^^^^^^^^^^^^^^

Die :text-info:`CSV Empfängerquelle` ließt die Empfänger aus einer CSV Datei, die in der Dateiverwaltung abgelegt wird.
Sie eignet sich vor allem zur einfachen Anbindung von CRM/ERP Systemen.

.. figure:: csv-einstellungen.png
   :scale: 100%
   :class: img-thumbnail img-responsive

.. figure:: csv-abonnenten.png
   :scale: 100%
   :class: img-thumbnail img-responsive

-----

Zufallsgenerator
^^^^^^^^^^^^^^^^

Der :text-info:`Zufallsgenerator` dient vorwiegend zum testen. Er erzeugt innerhalb einer definierten Menge, eine
zufällige Anzahl an Emfpängern, die bei jeder Abfrage unterschiedlich sind.

.. figure:: zufallsgenerator-einstellungen.png
   :scale: 100%
   :class: img-thumbnail img-responsive

.. figure:: zufallsgenerator-abonnenten.png
   :scale: 100%
   :class: img-thumbnail img-responsive

-----

Integrierte Abonnentenverwaltung
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. cssclass:: alert alert-info

   **Hinweis:** Die Komponente ``avisota/contao-subscription-recipient`` ist hierfür erforderlich.

Der :text-info:`Integrierte Abonnentenverwaltung` liefert die Abonnenten, die sich über die integrierte
Abonnentenverwaltung angemeldet haben.

.. figure:: integrierte-abonnentenverwaltung-einstellungen.png
   :scale: 100%
   :class: img-thumbnail img-responsive

.. figure:: integrierte-abonnentenverwaltung-abonnenten.png
   :scale: 100%
   :class: img-thumbnail img-responsive

.. cssclass:: alert alert-info
.. compound::

   **Hinweis**: Will man die Abonnenten nach ihrem Mailinglisten Abonnement auswählen, muss man dies in den Filtern
   aktivieren!

   .. figure:: integrierte-abonnentenverwaltung-filter.png
      :scale: 100%
      :class: img-thumbnail img-responsive
