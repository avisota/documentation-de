Support erhalten
================

Im Contao Backend gibt es im *Avisota* Abschnitt einen Menüpunkt *Support*.

.. figure:: backend_support.png
   :alt: Contao Backend - Menüpunkt Avisota > Support
   :class: img-thumbnail img-responsive

Über diesen Menüpunkt ist es möglich, Supportleistungen von den `Avisota Supportern`_ zu erhalten zu erhalten.

.. _Avisota Supportern: http://avisota.org/de/netzwerk/partner#supporter
