.. _contao-mailings:

Mailings anlegen und versenden
==============================

.. |newsletterkategorien| replace:: **Newsletterkategorien**
.. _newsletterkategorien: newsletterkategorien/

.. cssclass:: row
.. compound::

   .. cssclass:: col-md-4
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |newsletterkategorien|_

         .. cssclass:: panel-body
         .. compound::

            Organisieren Sie Ihre Mailings in Kategorien, um so den Überblick zu behalten.

Table of Contents
-----------------

.. toctree::
   :maxdepth: 2
   :titlesonly:

   newsletterkategorien/index