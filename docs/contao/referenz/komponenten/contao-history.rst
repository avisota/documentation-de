.. _contao-referenz-avisota/contao-history:

Historie
^^^^^^^^

.. cssclass:: sidebar row
.. compound::

   .. cssclass:: col-xs-6
   .. compound::

      **Status** (:ref:`? <faq_status>`)

      .. figure:: https://poser.pugx.org/avisota/contao-history/v/stable.png
         :alt: Latest Stable Version
         :target: https://packagist.org/packages/avisota/contao-history

      .. figure:: https://poser.pugx.org/avisota/contao-history/v/unstable.png
         :alt: Latest Unstable Version
         :target: https://packagist.org/packages/avisota/contao-history

      .. figure:: https://poser.pugx.org/avisota/contao-history/downloads.png
         :alt: Total Downloads
         :target: https://packagist.org/packages/avisota/contao-history

      .. figure:: https://poser.pugx.org/avisota/contao-history/license.png
         :alt: License
         :target: https://packagist.org/packages/avisota/contao-history

   .. cssclass:: col-xs-6
   .. compound::

      **Tracker** (:ref:`? <faq_tracker>`)

      .. figure:: ../../_static/images/jira_badge.png
         :alt: Issue tracker
         :target: https://avisota.atlassian.net/browse/CTOSTAT

      **Sources** (:ref:`? <faq_sources>`)

      .. figure:: ../../_static/images/bitbucket_badge.png
         :alt: Source on bitbucket
         :target: http://git.avisota.org/contao-history

      .. figure:: ../../_static/images/github_badge.png
         :alt: Source on github
         :target: https://github.com/avisota/contao-history

Einfache Versandhistorie und Statistik über versendete Nachrichten ohne Tracking.
