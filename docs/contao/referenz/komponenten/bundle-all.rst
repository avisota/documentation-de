.. _contao-referenz-avisota/contao-bundle-all:

Komplettpaket
^^^^^^^^^^^^^

.. cssclass:: sidebar row
.. compound::

   .. cssclass:: col-xs-6
   .. compound::

      **Status** (:ref:`? <faq_status>`)

      .. figure:: https://poser.pugx.org/avisota/contao-bundle-all/v/stable.png
         :alt: Latest Stable Version
         :target: https://packagist.org/packages/avisota/contao-bundle-all

      .. figure:: https://poser.pugx.org/avisota/contao-bundle-all/v/unstable.png
         :alt: Latest Unstable Version
         :target: https://packagist.org/packages/avisota/contao-bundle-all

      .. figure:: https://poser.pugx.org/avisota/contao-bundle-all/downloads.png
         :alt: Total Downloads
         :target: https://packagist.org/packages/avisota/contao-bundle-all

      .. figure:: https://poser.pugx.org/avisota/contao-bundle-all/license.png
         :alt: License
         :target: https://packagist.org/packages/avisota/contao-bundle-all

   .. cssclass:: col-xs-6
   .. compound::

      **Tracker** (:ref:`? <faq_tracker>`)

      .. figure:: ../../_static/images/jira_badge.png
         :alt: Issue tracker
         :target: https://avisota.atlassian.net/browse/CTO

      **Sources** (:ref:`? <faq_sources>`)

      .. figure:: ../../_static/images/bitbucket_badge.png
         :alt: Source on bitbucket
         :target: http://git.avisota.org/contao-bundle-all

      .. figure:: ../../_static/images/github_badge.png
         :alt: Source on github
         :target: https://github.com/avisota/contao-bundle-all

Ein Bundle, dass alle Contao Pakete umfasst.

- :ref:`avisota/contao-core <contao-referenz-avisota/contao-core>`
- :ref:`avisota/contao-message <contao-referenz-avisota/contao-message>`
- :ref:`avisota/contao-message-element-article <contao-referenz-avisota/contao-message-element-article>`
- :ref:`avisota/contao-message-element-download <contao-referenz-avisota/contao-message-element-download>`
- :ref:`avisota/contao-message-element-downloads <contao-referenz-avisota/contao-message-element-downloads>`
- :ref:`avisota/contao-message-element-event <contao-referenz-avisota/contao-message-element-event>`
- :ref:`avisota/contao-message-element-gallery <contao-referenz-avisota/contao-message-element-gallery>`
- :ref:`avisota/contao-message-element-headline <contao-referenz-avisota/contao-message-element-headline>`
- :ref:`avisota/contao-message-element-hyperlink <contao-referenz-avisota/contao-message-element-hyperlink>`
- :ref:`avisota/contao-message-element-image <contao-referenz-avisota/contao-message-element-image>`
- :ref:`avisota/contao-message-element-list <contao-referenz-avisota/contao-message-element-list>`
- :ref:`avisota/contao-message-element-news <contao-referenz-avisota/contao-message-element-news>`
- :ref:`avisota/contao-message-element-table <contao-referenz-avisota/contao-message-element-table>`
- :ref:`avisota/contao-message-element-text <contao-referenz-avisota/contao-message-element-text>`
- :ref:`avisota/contao-message-renderer-mailchimp <contao-referenz-avisota/contao-message-renderer-mailchimp>`
- :ref:`avisota/contao-history <contao-referenz-avisota/contao-history>`
- :ref:`avisota/contao-salutation <contao-referenz-avisota/contao-salutation>`
- :ref:`avisota/contao-subscription <contao-referenz-avisota/contao-subscription>`
- :ref:`avisota/contao-subscription-notification-center-bridge <contao-referenz-avisota/contao-subscription-notification-center-bridge>`
- :ref:`avisota/contao-subscription-member <contao-referenz-avisota/contao-subscription-member>`
- :ref:`avisota/contao-subscription-recipient <contao-referenz-avisota/contao-subscription-recipient>`
