.. _contao-referenz-avisota/contao-core:

Contao Grundsystem
^^^^^^^^^^^^^^^^^^

.. cssclass:: sidebar row
.. compound::

   .. cssclass:: col-xs-6
   .. compound::

      **Status** (:ref:`? <faq_status>`)

      .. figure::   https://poser.pugx.org/avisota/contao-core/v/stable.png
         :alt: Latest Stable Version
         :target: https://packagist.org/packages/avisota/contao-core

      .. figure::   https://poser.pugx.org/avisota/contao-core/v/unstable.png
         :alt: Latest Unstable Version
         :target: https://packagist.org/packages/avisota/contao-core

      .. figure::   https://poser.pugx.org/avisota/contao-core/downloads.png
         :alt: Total Downloads
         :target: https://packagist.org/packages/avisota/contao-core

      .. figure::   https://poser.pugx.org/avisota/contao-core/license.png
         :alt: License
         :target: https://packagist.org/packages/avisota/contao-core

   .. cssclass:: col-xs-6
   .. compound::

      **Tracker** (:ref:`? <faq_tracker>`)

      .. figure::   ../../_static/images/jira_badge.png
         :alt: Issue tracker
         :target: https://avisota.atlassian.net/browse/CTO

      **Sources** (:ref:`? <faq_sources>`)

      .. figure::  ../../_static/images/bitbucket_badge.png
         :alt: Source on bitbucket
         :target: http://git.avisota.org/contao-core

      .. figure::  ../../_static/images/github_badge.png
         :alt: Source on github
         :target: https://github.com/avisota/contao-core

Die Grundintegration für das Contao CMS. Erlaubt die Pflege und Konfiguration von Verteilern, Empfängerquellen,
Warteschlangen und Transportsystemen direkt aus dem Backend von Contao heraus. Für andere Erweiterungen werden diese
als Services bereitgestellt.
