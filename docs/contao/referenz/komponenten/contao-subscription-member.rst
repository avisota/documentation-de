.. _contao-referenz-avisota/contao-subscription-member:

Contao Mitglieder Abonnements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. cssclass:: sidebar row
.. compound::

   .. cssclass:: col-xs-6
   .. compound::

      **Status** (:ref:`? <faq_status>`)

      .. figure:: https://poser.pugx.org/avisota/contao-subscription-member/v/stable.png
         :alt: Latest Stable Version
         :target: https://packagist.org/packages/avisota/contao-subscription-member

      .. figure:: https://poser.pugx.org/avisota/contao-subscription-member/v/unstable.png
         :alt: Latest Unstable Version
         :target: https://packagist.org/packages/avisota/contao-subscription-member

      .. figure:: https://poser.pugx.org/avisota/contao-subscription-member/downloads.png
         :alt: Total Downloads
         :target: https://packagist.org/packages/avisota/contao-subscription-member

      .. figure:: https://poser.pugx.org/avisota/contao-subscription-member/license.png
         :alt: License
         :target: https://packagist.org/packages/avisota/contao-subscription-member

   .. cssclass:: col-xs-6
   .. compound::

      **Tracker** (:ref:`? <faq_tracker>`)

      .. figure:: ../../_static/images/jira_badge.png
         :alt: Issue tracker
         :target: https://avisota.atlassian.net/browse/CTOSUB

      **Sources** (:ref:`? <faq_sources>`)

      .. figure:: ../../_static/images/bitbucket_badge.png
         :alt: Source on bitbucket
         :target: http://git.avisota.org/contao-subscription-member

      .. figure:: ../../_static/images/github_badge.png
         :alt: Source on github
         :target: https://github.com/avisota/contao-subscription-member

Ermöglicht das Abonnieren von Mailinglisten durch Contao Mitglieder.
