.. _contao-referenz-avisota/contao-message-renderer-mailchimp:

MailChimp Renderer
^^^^^^^^^^^^^^^^^^

.. cssclass:: sidebar row
.. compound::

   .. cssclass:: col-xs-6
   .. compound::

      **Status** (:ref:`? <faq_status>`)

      .. figure:: https://poser.pugx.org/avisota/contao-message-renderer-mailchimp/v/stable.png
         :alt: Latest Stable Version
         :target: https://packagist.org/packages/avisota/contao-message-renderer-mailchimp

      .. figure:: https://poser.pugx.org/avisota/contao-message-renderer-mailchimp/v/unstable.png
         :alt: Latest Unstable Version
         :target: https://packagist.org/packages/avisota/contao-message-renderer-mailchimp

      .. figure:: https://poser.pugx.org/avisota/contao-message-renderer-mailchimp/downloads.png
         :alt: Total Downloads
         :target: https://packagist.org/packages/avisota/contao-message-renderer-mailchimp

      .. figure:: https://poser.pugx.org/avisota/contao-message-renderer-mailchimp/license.png
         :alt: License
         :target: https://packagist.org/packages/avisota/contao-message-renderer-mailchimp

   .. cssclass:: col-xs-6
   .. compound::

      **Tracker** (:ref:`? <faq_tracker>`)

      .. figure:: ../../_static/images/jira_badge.png
         :alt: Issue tracker
         :target: https://avisota.atlassian.net/browse/CTOMR

      **Sources** (:ref:`? <faq_sources>`)

      .. figure:: ../../_static/images/bitbucket_badge.png
         :alt: Source on bitbucket
         :target: http://git.avisota.org/contao-message-renderer-mailchimp

      .. figure:: ../../_static/images/github_badge.png
         :alt: Source on github
         :target: https://github.com/avisota/contao-message-renderer-mailchimp

Erzeugt das Mailing auf Basis der `Email Blueprints <https://github.com/mailchimp/Email-Blueprints>`_ die von MailChimp bereitgestellt werden.
