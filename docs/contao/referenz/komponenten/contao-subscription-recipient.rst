.. _contao-referenz-avisota/contao-subscription-recipient:

Unabhängige Abonnentenverwaltung
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. cssclass:: sidebar row
.. compound::

   .. cssclass:: col-xs-6
   .. compound::

      **Status** (:ref:`? <faq_status>`)

      .. figure:: https://poser.pugx.org/avisota/contao-subscription-recipient/v/stable.png
         :alt: Latest Stable Version
         :target: https://packagist.org/packages/avisota/contao-subscription-recipient

      .. figure:: https://poser.pugx.org/avisota/contao-subscription-recipient/v/unstable.png
         :alt: Latest Unstable Version
         :target: https://packagist.org/packages/avisota/contao-subscription-recipient

      .. figure:: https://poser.pugx.org/avisota/contao-subscription-recipient/downloads.png
         :alt: Total Downloads
         :target: https://packagist.org/packages/avisota/contao-subscription-recipient

      .. figure:: https://poser.pugx.org/avisota/contao-subscription-recipient/license.png
         :alt: License
         :target: https://packagist.org/packages/avisota/contao-subscription-recipient

   .. cssclass:: col-xs-6
   .. compound::

      **Tracker** (:ref:`? <faq_tracker>`)

      .. figure:: ../../_static/images/jira_badge.png
         :alt: Issue tracker
         :target: https://avisota.atlassian.net/browse/CTOSUB

      **Sources** (:ref:`? <faq_sources>`)

      .. figure:: ../../_static/images/bitbucket_badge.png
         :alt: Source on bitbucket
         :target: http://git.avisota.org/contao-subscription-recipient

      .. figure:: ../../_static/images/github_badge.png
         :alt: Source on github
         :target: https://github.com/avisota/contao-subscription-recipient

Eigenständige Mitgliederverwaltung mit Abonnement von Mailinglisten.
