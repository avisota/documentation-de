.. _contao-referenz-avisota/contao-subscription-notification-center-bridge:

NotificationCenter Bridge
~~~~~~~~~~~~~~~~~~~~~~~~~

.. cssclass:: sidebar row
.. compound::

   .. cssclass:: col-xs-6
   .. compound::

      **Status** (:ref:`? <faq_status>`)

      .. figure:: https://poser.pugx.org/avisota/contao-subscription-notification-center-bridge/v/stable.png
         :alt: Latest Stable Version
         :target: https://packagist.org/packages/avisota/contao-subscription-notification-center-bridge

      .. figure:: https://poser.pugx.org/avisota/contao-subscription-notification-center-bridge/v/unstable.png
         :alt: Latest Unstable Version
         :target: https://packagist.org/packages/avisota/contao-subscription-notification-center-bridge

      .. figure:: https://poser.pugx.org/avisota/contao-subscription-notification-center-bridge/downloads.png
         :alt: Total Downloads
         :target: https://packagist.org/packages/avisota/contao-subscription-notification-center-bridge

      .. figure:: https://poser.pugx.org/avisota/contao-subscription-notification-center-bridge/license.png
         :alt: License
         :target: https://packagist.org/packages/avisota/contao-subscription-notification-center-bridge

   .. cssclass:: col-xs-6
   .. compound::

      **Tracker** (:ref:`? <faq_tracker>`)

      .. figure:: ../../_static/images/jira_badge.png
         :alt: Issue tracker
         :target: https://avisota.atlassian.net/browse/CTOSUB

      **Sources** (:ref:`? <faq_sources>`)

      .. figure:: ../../_static/images/bitbucket_badge.png
         :alt: Source on bitbucket
         :target: http://git.avisota.org/contao-subscription-notification-center-bridge

      .. figure:: ../../_static/images/github_badge.png
         :alt: Source on github
         :target: https://github.com/avisota/contao-subscription-notification-center-bridge

Bridge für das `NotificationCenter <https://github.com/terminal42/contao-notification_center>`_ um Benachrichtigungen
beim Anmelden, Bestätigen und Abmelden von Abonnenten zu verschicken.
