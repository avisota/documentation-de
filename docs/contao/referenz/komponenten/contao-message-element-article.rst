.. _contao-referenz-avisota/contao-message-element-article:

Inhaltselement: Artikel
^^^^^^^^^^^^^^^^^^^^^^^

.. cssclass:: sidebar row
.. compound::

   .. cssclass:: col-xs-6
   .. compound::

      **Status** (:ref:`? <faq_status>`)

      .. figure::  https://poser.pugx.org/avisota/contao-message-element-article/v/stable.png
         :alt: Latest Stable Version
         :target: https://packagist.org/packages/avisota/contao-message-element-article

      .. figure::  https://poser.pugx.org/avisota/contao-message-element-article/v/unstable.png
         :alt: Latest Unstable Version
         :target: https://packagist.org/packages/avisota/contao-message-element-article

      .. figure::  https://poser.pugx.org/avisota/contao-message-element-article/downloads.png
         :alt: Total Downloads
         :target: https://packagist.org/packages/avisota/contao-message-element-article

      .. figure::  https://poser.pugx.org/avisota/contao-message-element-article/license.png
         :alt: License
         :target: https://packagist.org/packages/avisota/contao-message-element-article

   .. cssclass:: col-xs-6
   .. compound::

      **Tracker** (:ref:`? <faq_tracker>`)

      .. figure::  ../../_static/images/jira_badge.png
         :alt: Issue tracker
         :target: https://avisota.atlassian.net/browse/CTOME

      **Sources** (:ref:`? <faq_sources>`)

      .. figure::  ../../_static/images/bitbucket_badge.png
         :alt: Source on bitbucket
         :target: http://git.avisota.org/contao-message-element-article

      .. figure::  ../../_static/images/github_badge.png
         :alt: Source on github
         :target: https://github.com/avisota/contao-message-element-article

Liefert das Mailing-Inhaltselement zum Einbinden von Artikeln.
