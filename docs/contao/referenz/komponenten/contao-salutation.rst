.. _contao-referenz-avisota/contao-salutation:

Automatische Anreden
^^^^^^^^^^^^^^^^^^^^

.. cssclass:: sidebar row
.. compound::

   .. cssclass:: col-xs-6
   .. compound::

      **Status** (:ref:`? <faq_status>`)

      .. figure:: https://poser.pugx.org/avisota/contao-salutation/v/stable.png
         :alt: Latest Stable Version
         :target: https://packagist.org/packages/avisota/contao-salutation

      .. figure:: https://poser.pugx.org/avisota/contao-salutation/v/unstable.png
         :alt: Latest Unstable Version
         :target: https://packagist.org/packages/avisota/contao-salutation

      .. figure:: https://poser.pugx.org/avisota/contao-salutation/downloads.png
         :alt: Total Downloads
         :target: https://packagist.org/packages/avisota/contao-salutation

      .. figure:: https://poser.pugx.org/avisota/contao-salutation/license.png
         :alt: License
         :target: https://packagist.org/packages/avisota/contao-salutation

   .. cssclass:: col-xs-6
   .. compound::

      **Tracker** (:ref:`? <faq_tracker>`)

      .. figure:: ../../_static/images/jira_badge.png
         :alt: Issue tracker
         :target: https://avisota.atlassian.net/browse/CTO

      **Sources** (:ref:`? <faq_sources>`)

      .. figure:: ../../_static/images/bitbucket_badge.png
         :alt: Source on bitbucket
         :target: http://git.avisota.org/contao-salutation

      .. figure:: ../../_static/images/github_badge.png
         :alt: Source on github
         :target: https://github.com/avisota/contao-salutation

Ermöglicht individuelle Anreden zu erstellen, die nach definierbaren Regeln angewendet werden.
