.. _contao-referenz-avisota/contao-subscription:

Abonnements
^^^^^^^^^^^

.. cssclass:: sidebar row
.. compound::

   .. cssclass:: col-xs-6
   .. compound::

      **Status** (:ref:`? <faq_status>`)

      .. figure:: https://poser.pugx.org/avisota/contao-subscription/v/stable.png
         :alt: Latest Stable Version
         :target: https://packagist.org/packages/avisota/contao-subscription

      .. figure:: https://poser.pugx.org/avisota/contao-subscription/v/unstable.png
         :alt: Latest Unstable Version
         :target: https://packagist.org/packages/avisota/contao-subscription

      .. figure:: https://poser.pugx.org/avisota/contao-subscription/downloads.png
         :alt: Total Downloads
         :target: https://packagist.org/packages/avisota/contao-subscription

      .. figure:: https://poser.pugx.org/avisota/contao-subscription/license.png
         :alt: License
         :target: https://packagist.org/packages/avisota/contao-subscription

   .. cssclass:: col-xs-6
   .. compound::

      **Tracker** (:ref:`? <faq_tracker>`)

      .. figure:: ../../_static/images/jira_badge.png
         :alt: Issue tracker
         :target: https://avisota.atlassian.net/browse/CTOSUB

      **Sources** (:ref:`? <faq_sources>`)

      .. figure:: ../../_static/images/bitbucket_badge.png
         :alt: Source on bitbucket
         :target: http://git.avisota.org/contao-subscription

      .. figure:: ../../_static/images/github_badge.png
         :alt: Source on github
         :target: https://github.com/avisota/contao-subscription

Ein unabhängiges Abonnement-Management-System, das die Abonnements von Mailinglisten steuert.
