.. _contao-referenz-avisota/contao-message:

Nachrichten Grundsystem ``avisota/contao-message``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. cssclass:: sidebar row
.. compound::

   .. cssclass:: col-xs-6
   .. compound::

      **Status** (:ref:`? <faq_status>`)

      .. figure::  https://poser.pugx.org/avisota/contao-message/v/stable.png
         :alt: Latest Stable Version
         :target: https://packagist.org/packages/avisota/contao-message

      .. figure::  https://poser.pugx.org/avisota/contao-message/v/unstable.png
         :alt: Latest Unstable Version
         :target: https://packagist.org/packages/avisota/contao-message

      .. figure::  https://poser.pugx.org/avisota/contao-message/downloads.png
         :alt: Total Downloads
         :target: https://packagist.org/packages/avisota/contao-message

      .. figure::  https://poser.pugx.org/avisota/contao-message/license.png
         :alt: License
         :target: https://packagist.org/packages/avisota/contao-message

   .. cssclass:: col-xs-6
   .. compound::

      **Tracker** (:ref:`? <faq_tracker>`)

      .. figure::  ../../_static/images/jira_badge.png
         :alt: Issue tracker
         :target: https://avisota.atlassian.net/browse/CTOME

      **Sources** (:ref:`? <faq_sources>`)

      .. figure::  ../../_static/images/bitbucket_badge.png
         :alt: Source on bitbucket
         :target: http://git.avisota.org/contao-message

      .. figure::  ../../_static/images/github_badge.png
         :alt: Source on github
         :target: https://github.com/avisota/contao-message

Ermöglicht das erstellen und pflegen von Mailings direkt aus dem Backend. Integriert auch das Grundsystem zum erzeugen
und versenden der Nachrichten.
