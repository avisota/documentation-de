.. _contao-referenz-komponenten:

Komponenten-Referenz
====================

Avisota ist unterteilt in verschiedene Komponenten / Pakete, die hier kurz erklärt werden.

Framework (avisota/core)
------------------------

Das Paket ``avisota/core`` beinhaltet das PHP Framework
zum einfachen Arbeiten mit Abonnenten und Nachrichten. Für mehr Informationen besuchen sie die
:ref:`Avisota Framework Dokumentation <framework-dokumentation>`.

Bundles
-------

.. include:: komponenten/bundle-all.rst

Basis
-----

.. include:: komponenten/contao-core.rst

Nachrichten
-----------

.. include:: komponenten/contao-message.rst
.. include:: komponenten/contao-message-element-article.rst
.. include:: komponenten/contao-message-element-download.rst
.. include:: komponenten/contao-message-element-downloads.rst
.. include:: komponenten/contao-message-element-event.rst
.. include:: komponenten/contao-message-element-gallery.rst
.. include:: komponenten/contao-message-element-headline.rst
.. include:: komponenten/contao-message-element-hyperlink.rst
.. include:: komponenten/contao-message-element-image.rst
.. include:: komponenten/contao-message-element-list.rst
.. include:: komponenten/contao-message-element-news.rst
.. include:: komponenten/contao-message-element-table.rst
.. include:: komponenten/contao-message-element-text.rst
.. include:: komponenten/contao-message-renderer-mailchimp.rst

Statistik und Tracking
----------------------

.. include:: komponenten/contao-history.rst

Abonnenten und Abonnements
--------------------------

.. include:: komponenten/contao-salutation.rst
.. include:: komponenten/contao-subscription.rst
.. include:: komponenten/contao-subscription-notification-center-bridge.rst
.. include:: komponenten/contao-subscription-member.rst
.. include:: komponenten/contao-subscription-recipient.rst
