.. _contao-installation-über-composer:

Installation über Composer
==========================

Avisota 2 lässt sich über den `Contao Composer Client <https://c-c-a.org/ueber-composer>`_ installieren. Dazu müssen
die die gewünschten Pakete (siehe :ref:`Komponenten-Referenz <contao-referenz-komponenten>`) oder für die schnelle
Installation ein Bundle installiert werden.

Für den Schnellstart empfehlen wir die Installation des `avisota/contao-bundle-all`_ Bundles.

.. _avisota/contao-bundle-all: https://packagist.org/packages/avisota/contao-bundle-all

Wechseln Sie dazu in die Paketverwaltung und Suchen Sie nach dem Paket ``avisota/contao-bundle-all``.

.. figure:: search-package.png
   :class: img-thumbnail img-responsive

.. figure:: select-package.png
   :class: img-thumbnail img-responsive

Folgend wählt man die gewünschte Version aus und klickt auf *Paket für die Installation vormerken*.

.. figure:: mark-package.png
   :class: img-thumbnail img-responsive

Mit einem Klick auf *Pakete aktualisieren* wird Avisota 2 dann installiert.

.. cssclass:: alert alert-info

**Hinweis**: Bei der Installation werden rund 70 Pakete installiert. Der Vorgang kann also einige Zeit in Anspruch nehmen.

.. figure:: update.png
   :class: img-thumbnail img-responsive

Im Anschluss an die Installation muss lediglich noch die Datenbank aktualisiert werden.

.. figure:: updated.png
   :class: img-thumbnail img-responsive

.. cssclass:: alert alert-info

**Hinweis**: Nach der Installation oder Aktualisierung kann es vorkommen, dass rote Fehlerboxen mit den Meldungen
*Could not create avisota recipient source service*, *Could not create avisota queue service* oder
*Could not create avisota transport service* erscheinen. Diese Meldungen sind in den meisten Fällen ein anzeichen
dafür, dass die Datenbank nicht aktuell ist. Nach dem Aktualisieren der Datenbank sollten diese Meldungen verschwinden.

Avisota 2 ist jetzt installiert und bereit zur :ref:`konfiguration <contao-konfiguration>`.
