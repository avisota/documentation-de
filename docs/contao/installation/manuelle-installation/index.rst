.. _contao-installation-manuell:

Manuelle Installation
=====================

Avisota 2 lässt sich manuell installieren. Dazu müssen die gewünschten Pakete von der `Avisota Website
<http://avisota.org/download>`_ heruntergeladen werden. In den meisten Fällen bietet sich der Button **jüngste
Release Versionen herunterladen** an. Man erhält dann eine ZIP Datei, die alle Pakete beinhaltet. Das entspricht
der Installation vom Bundle ``avisota/contao-bundle-all`` über Composer.

.. cssclass:: alert alert-warning

**Hinweis**: Die ZIP Datei über alle Versionen wird live generiert, es kann also einen Moment dauern, bevor der
Download startet.

.. figure:: download.png
   :class: img-thumbnail img-responsive

Die ZIP Datei beinhaltet jetzt alle Dateien aus allen Dateien und kann so in eine Contao Installation entpackt werden.

.. figure:: zip.png
   :class: img-thumbnail img-responsive

.. cssclass:: alert alert-info

**Tipp**: Die Datei ``system/modules/AVISOTA_RELEASES`` beinhaltet eine Liste aller Pakete und deren Versionen, die
in dem ZIP Archiv enthalten waren. Diese Datei ist allerdings nur in dem Komplettdownload verfügbar!

.. figure:: all-releases-information.png
   :class: img-thumbnail img-responsive

.. cssclass:: alert alert-info

**Tipp**: Jedes Paket beinhaltet eine ``system/modules/.../RELEASE`` Datei die ebenfalls alle Versionsinformationen
beinhaltet. Außerdem sind dort noch weitere Meta Informationen, wie URL, Versionsnummer, Commit Hash und Release-Datum
enthalten.

.. figure:: package-release-information.png
   :class: img-thumbnail img-responsive

Jetzt muss man nur noch die ZIP Datei in die Contao Installation entpacken.

Hat man die Dateien entpackt, muss man noch das Datenbankupdate durchführen. Dies geschieht entweder über das
*Install-Tool* oder die *Erweiterungsverwaltung*.

.. figure:: database-update.png
   :class: img-thumbnail img-responsive

.. cssclass:: alert alert-info

**Hinweis**: Nach der Installation oder Aktualisierung kann es vorkommen, dass rote Fehlerboxen mit den Meldungen
*Could not create avisota recipient source service*, *Could not create avisota queue service* oder
*Could not create avisota transport service* erscheinen. Diese Meldungen sind in den meisten Fällen ein anzeichen
dafür, dass die Datenbank nicht aktuell ist. Nach dem Aktualisieren der Datenbank sollten diese Meldungen verschwinden.

Avisota 2 ist jetzt installiert und bereit zur :ref:`konfiguration <contao-konfiguration>`.
