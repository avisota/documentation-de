.. _contao-installation:

Installation
============

.. |installation-über-composer| replace:: **Installation über Composer**
.. _installation-über-composer: über-composer/

.. |manuelle-installation| replace:: **Manuelle Installation**
.. _manuelle-installation: manuelle-installation/

.. cssclass:: row
.. compound::

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |installation-über-composer|_

         .. cssclass:: panel-body
         .. compound::

            Einfache Installation mittels Composer.

   .. cssclass:: col-md-6
   .. compound::

      .. cssclass:: panel panel-default
      .. compound::

         .. cssclass:: panel-heading

         |manuelle-installation|_

         .. cssclass:: panel-body
         .. compound::

            Manuelle Installation, falls Composer nicht verwendet werden kann.

.. _contao-coult-not-create-service-errors:
.. cssclass:: alert alert-info
.. compound::

   **Hinweis**: Nach der Installation oder Aktualisierung kann es vorkommen, dass rote Fehlerboxen mit den Meldungen
   ``Could not create avisota recipient source service``, ``Could not create avisota queue service`` oder
   ``Could not create avisota transport service`` erscheinen.

   Diese Meldungen sind erst einmal unkritisch und in den meisten Fällen ein anzeichen dafür, dass die Datenbank nicht
   aktuell ist. Nach dem Aktualisieren der Datenbank sollten diese Meldungen verschwinden.

Table of Contents
-----------------

.. toctree::
   :maxdepth: 2
   :titlesonly:

   über-composer/index
   manuelle-installation/index
